#include <stdio.h>

#define MAXLINE 5

int getline_j(char line[], int maxline, int *flag);
void copy(char to[], char from[]);

main( )
{
    int len;
    int max;
    int flag = 0;
    char line[MAXLINE];
    char longest[MAXLINE];

    max = 0;
    while ((len = getline_j(line, MAXLINE, &flag)) > 0 )
        if (len >=  max) {
            max = len;
            copy(longest, line);
        }
        if (max > 0)
            printf("%s", longest);
        return 0;
}

int getline_j(char s[], int lim, int *flag) {
    int c, i;
    
    for (i=0; i<lim-1 && (c=getchar())!=EOF && c!='\n' ; ++i) {
        if (*flag == 0){
            printf("%c\n",c);
            s[i] = c;
        }
    }
    if (i<lim-1 && c == '\n') {
        printf("just received \\n");
        s[i] = c;
        ++i;
        *flag = 0;
       }
    else {
        s[--i] = '\n';
        ++i;
        *flag = 1;
    }

    s[i] = '\0';
    
    printf("The length of input is: %d\n", i);


    return i;
}

void copy(char to[], char from[]) {
    int i;

    i = 0 ;

    printf("Observed longer word, copying it over: %s", from);
    while ((to[i] = from[i]) != '\0')
        ++i;
}

