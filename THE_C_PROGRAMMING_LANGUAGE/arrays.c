#include <stdio.h>

#define TOTAL_CHARACTER_NUMBER 128

/* count digits, white space, others */
main()
{
	int c, i, j, nwhite, nother, wcount, done;
	int ndigit[10], character_count[TOTAL_CHARACTER_NUMBER];

	wcount = nwhite = nother = 0;
	done = 1;

	for (i = 0; i < 10; ++i){
		ndigit[i] = 0;	
	}
	for (i = 0; i < TOTAL_CHARACTER_NUMBER; ++i){
		character_count[i] = 0;	
	}
	while ((c = getchar()) != EOF){
		++character_count[c];
		if (c == ' ' || c == '\n' || c == '\t'){
			++ndigit[wcount-1];
			wcount = 0;
		}
		else
			++wcount;
	}
    if (c == EOF){
		++ndigit[wcount-1];
		wcount = 0;
    }
//	printf("digits =");
    // Horizontal histogram
    printf("\n");
	for (i = 0; i < 10; ++i){
		printf("%d word/s count: ", i + 1);
		for (j = 0; j < ndigit[i]; ++j)
			printf("-");
		printf("\n");
	}
	// Vertical histogram
	printf("\n");
	for (i = 0; i < 10; ++i)
		printf("%d", i+1);
	printf("\n");
	while (done != 0){
		done = 0;
		for (i = 0; i < 10; ++i){
			if (ndigit[i] > 0){
				printf("H");
				--ndigit[i];
				++done;
			}
			else
			printf(" ");
		}
		printf("\n");
	}
	for (i = 0; i < TOTAL_CHARACTER_NUMBER; ++i){
		putchar(i);
		for (j=0; j < character_count[i]; ++j)
			putchar('*');
		putchar('\n');
	}
}