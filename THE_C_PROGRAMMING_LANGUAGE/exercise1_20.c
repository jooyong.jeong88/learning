#include <stdio.h>

#define TAB 4
#define MAXLINE 4

int main() 
{
    char chars[MAXLINE];
    int in;
    int i;

    i = 0;
    while ((in=getchar()) != EOF) {
        chars[i++] = in;
        printf("Input is: %s", chars);
    }
 
    return 0;
}

