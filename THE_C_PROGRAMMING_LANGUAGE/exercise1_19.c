#include <stdio.h>

#define MAXLINE 100000

int getline_j(char line[], int maxline);
void reverse(char to[], char from[], int len);

int main( )
{
    int len;
    char line[MAXLINE];

    while ((len=getline_j(line, MAXLINE)) > 0);
    return 0;
}

int getline_j(char s[], int lim) {
    int c, i;
    char r[MAXLINE];
    
    for (i=0; i<lim-1 && (c=getchar())!=EOF && c!='\n' ; ++i) {
            s[i] = c;
        }
    if (c == '\n') {
        s[i] = c;
        ++i;
       }
    s[i] = '\0';
    if (c!=EOF) {
        printf("Input given is: %s", s);
        reverse(r, s, i);
        printf("Reversed input is %s", r);
    }
    return i;
}

void reverse(char to[], char from[], int len) {
    int i = 0;
    int j;
    
    for(j = len-2; j >= 0; --j) {
        to[i++] = from[j];
        printf("%c", from[j]);
    }
    
    to[i++] = '\n';
    to[i] = '\0';
}
