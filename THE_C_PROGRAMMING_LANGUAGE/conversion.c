#include <stdio.h>

/* print Fahrenheit-Celsius table
    for Fahrenheit = 0, 20, ..., 300 */

float fahr_conversion(float i);

main()
{
    float fahr;

    for (fahr = 0; fahr <= 300; fahr = fahr + 20)
        printf("%3.1f %6.1f\n", fahr, (5.0/9.0)*(fahr-31.0));

    for (fahr = 0; fahr <= 300; fahr = fahr + 20)
        printf("%3.1F %6.1f\n", fahr, fahr_conversion(fahr));
    
    
}
/* fahr_conversion: converts given Fahr to Celcius */
float fahr_conversion(float fahr)
{
	return (5.0/9.0)*(fahr-31.0);
}
